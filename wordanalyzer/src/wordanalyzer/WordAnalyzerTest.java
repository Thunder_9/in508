package wordanalyzer;

import org.junit.Assert;
import org.junit.Test;



public class WordAnalyzerTest {

	@Test
	public void test() {
		
	





   String s1="aardvark"; // expect: a
   String s2=("roommate"); // expect: o (not m)
   String s3=("mate"); // expect: 0 (no duplicate letters)
   String s4=("test"); // expect: 0 (the t isn't repeating)



   WordAnalyzer wa = new WordAnalyzer(s1);
  //char result = wa.firstRepeatedCharacter();
  
   WordAnalyzer wb = new WordAnalyzer(s2);
   WordAnalyzer wc = new WordAnalyzer(s3);
   WordAnalyzer wd = new WordAnalyzer(s4);
   char result = wa.firstRepeatedCharacter();
 
   Assert.assertEquals("a", result); 
   result = wb.firstRepeatedCharacter();
   Assert.assertEquals("o", result);
   result = wc.firstRepeatedCharacter();
   Assert.assertEquals(0, result);
   result = wd.firstRepeatedCharacter();
   Assert.assertEquals(0, result);}




@Test 
public void test2()
   {
      String s1= ("missisippi"); // expect: i (not m or s)
      String s2=("mate"); // expect: 0 (no duplicate letters)
      String s3=("test"); // expect: t
   

   
   
      WordAnalyzer wb = new WordAnalyzer(s2);
      WordAnalyzer wc = new WordAnalyzer(s3);
      WordAnalyzer wa = new WordAnalyzer(s1);
      char result = wa.firstRepeatedCharacter();
    
      Assert.assertEquals("i", result); 
      result = wb.firstRepeatedCharacter();
      Assert.assertEquals(0, result);
      result = wc.firstRepeatedCharacter();
      Assert.assertEquals("t", result);
 
}

@Test
public void test3()
{
	String s1=("mississippiii"); // expect: 4 (ss, ss, pp, iii)
	String s2=("test"); // expect: 0 (no repeated letters)
	String s3=("aabbcdaaaabb"); // expect: 4 (aa, bb, aaaa, bb)



    WordAnalyzer wb = new WordAnalyzer(s2);
    WordAnalyzer wc = new WordAnalyzer(s3);
    WordAnalyzer wa = new WordAnalyzer(s1);
    char result = wa.firstRepeatedCharacter();
  
    Assert.assertEquals(4, result); 
    result = wb.firstRepeatedCharacter();
    Assert.assertEquals(0, result);
    result = wc.firstRepeatedCharacter();
    Assert.assertEquals(4, result);


}
}
