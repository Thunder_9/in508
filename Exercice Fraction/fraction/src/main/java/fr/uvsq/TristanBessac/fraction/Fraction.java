package fr.uvsq.TristanBessac.fraction;

public final class Fraction {
	
	private	final int  numerateur;
	private	final int denominateur;
    private final static Fraction ZERO;
    private final static Fraction UN;
    static {
        ZERO = new Fraction(0,1);
        UN = new Fraction(1,1);
    }

	public Fraction(){
		this.numerateur=0;
		this.denominateur=1;
	}

    public Fraction(int num, int den){
        this.numerateur = num;
        this.denominateur = den;
    }

    public Fraction(int num){
        this.numerateur = num;
        this.denominateur = 1;
    }

	public void get_num(){
		System.out.println(this.numerateur);
	}

	public void get_denom(){
		System.out.println(this.denominateur);
	}

	public void eval() {
        double fraction = this.numerateur / this.denominateur;
        System.out.println(fraction);
    }

    public void ConversionEnString(){
	    String n = Integer.toString(numerateur);
        String d = Integer.toString(denominateur);

        System.out.println(n, " sur ", d);
    }
}
